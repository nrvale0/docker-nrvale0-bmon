# docker-nrvale0-bmon

A container image for the bmon network monitoring utility.

## Usage

```pre
$ docker run --rm --network=host -ti -e TERM=$TERM nrvale0/bmon
```

There is also a script, 'bmon.sh', in the repo which can be linked to/copied into your path which does the same.

## Resources

| description | repo |
| --- | --- |
| repo for this Docker image | https://gitlab.com/nrvale0/docker-nrvale0-bmon |
| repo for bmon | https://github.com/nrvale0/bmon |
| Docker image which influenced this work | https://hub.docker.com/r/sthysel/bmon/~/dockerfile/ |

## Support, Issues, Patches

* Nathan Valentine &lt;[nrvale00@gmail.com](mailto:nrvale00@gmail.com)&gt;
* https://gitlab.com/nrvale0/docker-nrvale0-bmon

