FROM ubuntu:latest as build

ENV PATH /bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin
ENV DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

RUN apt-get update

RUN apt-get install -y \
  ca-certificates \
  apt-transport-https \
  build-essential \
  git \
  autoconf \
  automake \
  make \
  pkg-config \
  libnl-route-3-200 \
  libconfuse-dev \
  libnl-3-dev \
  libnl-route-3-dev \
  libncurses-dev

RUN mkdir -p /tmp/src/ && \
  git clone https://github.com/tgraf/bmon.git /tmp/src/bmon

RUN cd /tmp/src/bmon && \
  ./autogen.sh && \
  ./configure  && \
  make && \
  make install

FROM ubuntu:latest
MAINTAINER Nathan Valentine <nrvale0@protonmail.com>
LABEL vendor="Nathan Valentine" \
      email="nrvale0@protonmail.com" \
      repo="https://gitlab.com/nrvale0/docker-nrvale0-bmon" \
      upstream="https://github.com/tgraf/bmon" \
      credits="https://hub.docker.com/r/sthysel/bmon/~/dockerfile/"
COPY --from=build /usr/local/bin/bmon /usr/local/bin/
COPY /Dockerfile /
WORKDIR /
ENTRYPOINT ["bmon"]
