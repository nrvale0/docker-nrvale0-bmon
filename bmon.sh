#!/bin/bash

set -eu

: ${TERM:=xterm-256color}

while true; do
  docker run --network=host --rm -ti -e TERM=$TERM nrvale0/bmon $@
done
